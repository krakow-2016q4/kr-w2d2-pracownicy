package com.szkolenie.klasy1;

import java.util.LinkedList;
import java.util.List;

public class Menedzer extends Pracownik {
    private List<PracownikLiniowy> pracownicy = new LinkedList<>();

    public Menedzer(String name) {
        super(name);
    }

    public List<PracownikLiniowy> getPracownicy() {
        return pracownicy;
    }

    public void setPracownicy(List<PracownikLiniowy> pracownicy) {
        this.pracownicy = pracownicy;
    }

    @Override
    public String toString() {
        return String.format("%s - Menedżer", getName());
    }
}
