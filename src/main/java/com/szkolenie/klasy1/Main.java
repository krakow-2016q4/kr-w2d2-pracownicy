package com.szkolenie.klasy1;

public class Main {
    public static void main(String[] args) {
        // 1. Wyswietlenie wszystkich pracownikow
        System.out.println(DB.getPracownicy());
        System.out.println();

        // 2. wyświetlenie wszystkich wpisów z rejestru dla zespołów pracowników
        // liniowych menadżera
        for (Menedzer menedzer : DB.getMenedzerowie()) {
            System.out.println(menedzer.getName() + "====================");
            for (PracownikLiniowy pl : menedzer.getPracownicy()) {
                System.out.println("\tPracownik: " + pl.getName());
                System.out.println("\tGodziny: " + pl.getGodziny());
                System.out.println("\tRejestr: " + pl.getRejestr());
                System.out.println("\t==================");
            }
        }
    }
}
